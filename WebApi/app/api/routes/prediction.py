from fastapi import APIRouter, Depends
from starlette.requests import Request

from app.models.payload import StarbucksPayload
from app.models.prediction import StarbucksPredictionResult
from app.services.models import StarbucksModel

router = APIRouter()


@router.post("/predict", response_model=StarbucksPredictionResult, name="predict")
def post_predict(
    request: Request, data: StarbucksPayload = None,
) -> StarbucksPredictionResult:

    model: StarbucksModel = request.app.state.model
    prediction: StarbucksPredictionResult = model.predict(data)

    return prediction
