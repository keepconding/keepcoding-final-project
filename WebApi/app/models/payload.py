from pydantic import BaseModel


class StarbucksPayload(BaseModel):
    maleLivingAlone: float
    femaleLivingAlone: float
    numberOfFamilies: float
    hispanicLatinoPopulation: float
    asianPopulation: float
    twoOrMoreRacesPopulation: float
    oneRacePopulation: float
    totalHouseHolders: float
    twoPersonFamilies: float
    oneEarnerFamilies: float
    population24_34: float
    population35_44: float
    houseHolders25_44: float
    houseHolders15_24: float
    totalHousingUnits: float
    numberOfStations: float
    numberOfSchools: float
    numberOfFashionShops: float
    numberOfStadiums: float
    numberOfHostelry: float
    numberOfParks: float
    numberOfHotels: float
    numberOfSpectacles: float


def payload_to_array(payload: StarbucksPayload) -> list:
    return [payload.maleLivingAlone,
            payload.femaleLivingAlone,
            payload.numberOfFamilies,
            payload.hispanicLatinoPopulation,
            payload.asianPopulation,
            payload.twoOrMoreRacesPopulation,
            payload.oneRacePopulation,
            payload.totalHouseHolders,
            payload.twoPersonFamilies,
            payload.oneEarnerFamilies,
            payload.population24_34,
            payload.population35_44,
            payload.houseHolders25_44,
            payload.houseHolders15_24,
            payload.totalHousingUnits,
            payload.numberOfStations,
            payload.numberOfSchools,
            payload.numberOfFashionShops,
            payload.numberOfStadiums,
            payload.numberOfHostelry,
            payload.numberOfParks,
            payload.numberOfHotels,
            payload.numberOfSpectacles]
