#!/usr/bin/env python
# coding: utf-8

# In[ ]:





# In[ ]:


def process_sociodemographic_data(data, future_prediction = False):
    if future_prediction == False:
        #remove outliers:
        data = data[data["revenue"] <= 4000000]

    #rename variables for better understanding:
    new_columns = {}
    for column in list(data.columns):
        new_c = str(column).replace('Estimate!!SEX AND AGE!!Total population!!','Population age:').replace('Estimate!!Number!!HOUSEHOLD INCOME BY RACE AND HISPANIC OR LATINO ORIGIN OF HOUSEHOLDER!!Households!!One race--!!','Householder by origin:')
        new_c = new_c.replace('Estimate!!Number!!FAMILY INCOME BY FAMILY SIZE!!','Family size').replace('Estimate!!Number!!HOUSEHOLD INCOME BY AGE OF HOUSEHOLDER!!','Householder by age:')
        new_c = new_c.replace('Estimate!!Number!!FAMILY INCOME BY NUMBER OF EARNERS!!','Number of earners:').replace('Estimate!!Number!!HOUSEHOLD INCOME BY RACE AND HISPANIC OR LATINO ORIGIN OF HOUSEHOLDER!!Households!!Hispanic or Latino origin (of any race)','Householder by origin: any race')
        new_c = new_c.replace('Estimate!!Number!!HOUSEHOLD INCOME BY RACE AND HISPANIC OR LATINO ORIGIN OF HOUSEHOLDER!!Households!!White alone, not Hispanic or Latino','Householder by origin: white people')
        new_c = new_c.replace('Estimate!!Number!!HOUSEHOLD INCOME BY RACE AND HISPANIC OR LATINO ORIGIN OF HOUSEHOLDER!!Households','Total householders')
        new_c = new_c.replace('Estimate!!Number!!HOUSEHOLD INCOME BY RACE AND HISPANIC OR LATINO ORIGIN OF HOUSEHOLDER!!Households!!Two or more races','Householders by origin: two or more races')
        new_c = new_c.replace('Estimate!!HISPANIC OR LATINO AND RACE!!Total population!!','Total population by race:')
        new_c = new_c.replace('Estimate!!Number!!NONFAMILY HOUSEHOLDS!!Nonfamily households!!','Nonfamiliy households:').replace('Estimate!!Number!!FAMILIES!!Families!!','Families type:')
        new_c = new_c.replace('Estimate!!CITIZIEN, VOTING AGE POPULATION!!Citizien','Voting by age:').replace('Estimate!!HISPANIC OR LATINO AND RACE!!Total population','Total hispanic or latino population')
        new_c = new_c.replace('Estimate!!Number!!FAMILIES!!Families','Number of families').replace('Estimate!!RACE!!Total population!!One race!!Asian','Total asian population')
        new_columns[column] = new_c
    data.rename(columns=new_columns,inplace=True)
    
    #keep the selected variables in the Exploratory Data Analysis
    main_columns = ['revenue', 'storenumber', 'Geographic Area Name','geometry',
       'Nonfamiliy households:Male householder!!Living alone',
       'Nonfamiliy households:Female householder!!Living alone',
       'Number of families', 'Total hispanic or latino population',
       'Total asian population',
       'Estimate!!RACE!!Total population!!Two or more races',
       'Estimate!!RACE!!Total population!!One race', 'Total householders',
       'Family size2-person families', 'Number of earners:1 earner',
       'Population age:25 to 34 years', 'Population age:35 to 44 years',
       'Householder by age:25 to 44 years',
       'Householder by age:15 to 24 years', 'Estimate!!Total housing units']
    data = data[main_columns]
    
    if future_prediction == False:
        #get the mean revenue for those Starbucks in the same geographic area (as they will hhave same features)
        data_groupBy = data.groupby(by='Geographic Area Name', as_index=False).mean()
        data_groupBy = data_groupBy[['revenue','Geographic Area Name']]

        data = data.drop(['revenue'], axis=1)
        data = data.merge(data_groupBy, left_on='Geographic Area Name', right_on='Geographic Area Name')
        data = data.drop_duplicates(subset=["Geographic Area Name"])

    #sort columns to have revenue in the first place
    #columns = list(data.columns)
    #columns = columns[:-1]
    #columns.insert(0,'revenue')
    #data = data[columns]

    
    return data


# In[ ]:


def process_placesInfo_data(data,future_prediction = False):
    if future_prediction == False:
        main_columns = ['storenumber']
    if future_prediction == True:
        main_columns = ['Geographic Area Name']
    
    selected_variables = ['stations', 'schools', 'fashion_shops', 'stadiums', 'hostelry', 'parks', 'hotels', 'spectacles']

    for x in selected_variables:
        main_columns.append(x)
        
    data = data[main_columns]

    return data


# In[1]:


def get_data_ready(data, future_prediction = False):
    columns_places = ['Geographic Area Name','storenumber', 'aerports', 'hostelry', 'schools', 'banks', 'sanity', 'spectacles', 'police_stations', 'temples', 'stadiums', 'stations', 'malls', 'fashion_shops', 'parks', 'gyms', 'hotels', 'attractions']
    data_places = data[columns_places]
    
    columns_demographics = set(data.columns[data.columns.str.startswith('Estimate!!')])
    columns_demographics.update(['revenue','Geographic Area Name', 'storenumber','geometry'])
    data_demographics = data[columns_demographics]
    
    data_places = process_placesInfo_data(data_places, future_prediction)
    data_demographics = process_sociodemographic_data(data_demographics, future_prediction)
    
    if future_prediction == False:
        data = data_demographics.merge(data_places, on='storenumber')
    else:
        data = data_demographics.merge(data_places, on='Geographic Area Name')

    return data


# In[ ]:




