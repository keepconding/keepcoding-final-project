import psycopg2
import sys
import boto3
import pandas as pd
import os
import logging
import time

LOG_LEVEL = logging.INFO
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=LOG_LEVEL)
logger = logging.getLogger(__name__)


# Connection with the bucket S3:

s3 = boto3.resource(
    service_name='s3',
    region_name='eu-west-1',
    aws_access_key_id='AKIAYUYRMI3JBU5ZNVW3',
    aws_secret_access_key='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'
)

# Get all the elements that we have in our bucket:
my_bucket = s3.Bucket('keepcoding')
files = [ my_bucket_object.key for my_bucket_object in my_bucket.objects.all()]

# Connection with the database Postgresql:

conn =  psycopg2.connect(
        host = "keepcoding.ceybdeqenyip.us-east-2.rds.amazonaws.com",
        database = "postgres",
        user ="JorgeSolaAsensio",
        password = "1Saviola1")


def checkDataframe(file, df):
    if file == 'new_york_starbucks.csv':
        df = df.drop(["Unnamed: 0","addresslines","geometry",'zipcode'], axis = 1)
        first_col = df.pop('storenumber')
        df.insert(0, 'storenumber', first_col)
        
    if file == 'new_york_zipcodes.csv':
        df = df.drop(["State","Timezone","Daylight savings time flag","geopoint"], axis = 1)
        df.rename(columns={"Zip": "Zipcode"},inplace=True)   
        
    if 'storenumber' in list(df.columns):
        df['storenumber'] = df['storenumber'].str.replace('-','0')
        df['storenumber'] = pd.to_numeric(df['storenumber'])
        
    return df       

def process_demographic_dataframe(df):
    
    # Keep the main columns. This columns reference the result of the analyze the social demographic dataset:
    columns = [  'id',
                 'Geographic Area Name',
                 'Estimate!!Number!!NONFAMILY HOUSEHOLDS!!Nonfamily households!!Male householder!!Living alone',
                 'Estimate!!Number!!NONFAMILY HOUSEHOLDS!!Nonfamily households!!Female householder!!Living alone',
                 'Estimate!!HISPANIC OR LATINO AND RACE!!Total population',
                 'Estimate!!Number!!HOUSEHOLD INCOME BY RACE AND HISPANIC OR LATINO ORIGIN OF HOUSEHOLDER!!Households',
                 'Estimate!!Number!!FAMILY INCOME BY FAMILY SIZE!!2-person families',
                 'Estimate!!Number!!FAMILY INCOME BY NUMBER OF EARNERS!!1 earner',
                 'Estimate!!SEX AND AGE!!Total population!!25 to 34 years',
                 'Estimate!!SEX AND AGE!!Total population!!35 to 44 years',
                 'Estimate!!Number!!HOUSEHOLD INCOME BY AGE OF HOUSEHOLDER!!25 to 44 years',
                 'Estimate!!Total housing units']
    
    df = df[columns]
    
    #Rename columns:

    new_columns = {}
    for column in list(df.columns):
        new_c = str(column).replace('Estimate!!SEX AND AGE!!Total population!!','Population age:').replace('Estimate!!Number!!HOUSEHOLD INCOME BY RACE AND HISPANIC OR LATINO ORIGIN OF HOUSEHOLDER!!Households!!One race--!!','Householder by origin:')
        new_c = new_c.replace('Estimate!!Number!!FAMILY INCOME BY FAMILY SIZE!!','Family size').replace('Estimate!!Number!!HOUSEHOLD INCOME BY AGE OF HOUSEHOLDER!!','Householder by age:')
        new_c = new_c.replace('Estimate!!Number!!FAMILY INCOME BY NUMBER OF EARNERS!!','Number of earners:').replace('Estimate!!Number!!HOUSEHOLD INCOME BY RACE AND HISPANIC OR LATINO ORIGIN OF HOUSEHOLDER!!Households!!Hispanic or Latino origin (of any race)','Householder by origin: any race')
        new_c = new_c.replace('Estimate!!Number!!HOUSEHOLD INCOME BY RACE AND HISPANIC OR LATINO ORIGIN OF HOUSEHOLDER!!Households!!White alone, not Hispanic or Latino','Householder by origin: white people')
        new_c = new_c.replace('Estimate!!Number!!HOUSEHOLD INCOME BY RACE AND HISPANIC OR LATINO ORIGIN OF HOUSEHOLDER!!Households','Total householders')
        new_c = new_c.replace('Estimate!!Number!!HOUSEHOLD INCOME BY RACE AND HISPANIC OR LATINO ORIGIN OF HOUSEHOLDER!!Households!!Two or more races','Householders by origin: two or more races')
        new_c = new_c.replace('Estimate!!HISPANIC OR LATINO AND RACE!!Total population!!','Total population by race:')
        new_c = new_c.replace('Estimate!!Number!!NONFAMILY HOUSEHOLDS!!Nonfamily households!!','Nonfamiliy households:').replace('Estimate!!Number!!FAMILIES!!Families!!','Families type:')
        new_c = new_c.replace('Estimate!!CITIZIEN, VOTING AGE POPULATION!!Citizien','Voting by age:').replace('Estimate!!HISPANIC OR LATINO AND RACE!!Total population','Total hispanic or latino population')
        new_columns[column] = new_c
        
    # Change the columns name to make easier read the data:
    df.rename(columns=new_columns,inplace=True)
    
    return df
    
def main_tables(df):
    index = [num for num in range(1, len(df.index) +1)]
    df['id'] = index
    
    # Creacion dataframe para la tabla starbucks
    df_shops = df[['id', 'storenumber']]
    df_shops = df_shops.dropna()
    
    # Creacion dataframe para la tabla de puntos de interés de New York
    df_new_tork_places = df[['id','aerports','hostelry','schools','banks','sanity','spectacles','police_stations','temples','stadiums','stations','malls','fashion_shops','parks','gyms','hotels','attractions']]
    
    # Creacion dataframe para la tabla de información social demográfica de New York    
    df_demographic = process_demographic_dataframe(df)
    df_demographic = df_demographic.drop(['Geographic Area Name'], axis = 1)
    
    # Creación dataframe identificación by zipcode
    df_location = df[['id','Geographic Area Name']]
    df_location.rename(columns={"Geographic Area Name": "Zipcode"},inplace=True)  
    
    tables = {
        'zipcode_data':df_location,
        'new_york_demographic':df_demographic,
        'new_york_places':df_new_tork_places,
        'starbucks':df_shops}
    
    return tables

 # This function download a .csv from the bucket and it converts into dataframe:

def download_csv_file(file):
    logger.info(f'Downloading file: {file}...')
    my_bucket.download_file(f'{file}',f'{file}')
    if file == 'new_york_zipcodes.csv':
        df = pd.read_csv(f'{file}',sep=';')
    else:
        df = pd.read_csv(f'{file}')
    os.remove(f'{file}')         
    return df

def save_data(table, data):
    
    t0 = time.time()
    logger.info(f'Process {table} data....' )
    cursor = conn.cursor()
    columns = [str(column).lower().replace(' ','_').replace('!!','_').replace(':','_').replace('-','_') for column in data[0].keys()]
    columns =  ",".join(columns)
    new_data = []
    for ix, row in enumerate(data):
        block = []
        block = [str(value).replace("'",'') for value in row.values()]
        block = str(block).replace('[','').replace(']','')
        new_data.append(block)
        if ix > 1 and ix % 500  == 0:
            values = "),(".join(new_data)
            sentence = f"INSERT INTO {table} ({columns}) VALUES ({values}) ON CONFLICT DO NOTHING" 
            cursor.execute(sentence)
            conn.commit()
            new_data = []
            logger.info(f'Total files processed: {ix}')
    
    logger.info(f'Total files processed: {ix}')
    
    if new_data:
        values = "),(".join(new_data)
        sentence = f"INSERT INTO {table} ({columns}) VALUES ({values}) ON CONFLICT DO NOTHING" 
        cursor.execute(sentence)
        conn.commit()
    logger.info(f'{table} finished')
    t1 = time.time()
    logger.info(f'Total time procese: {t1 - t0}')

def main_load():

    tables = {
    'new_york_zipcodes':'new_york_zipcodes.csv',
    'new_york_starbucks_information':'new_york_starbucks.csv',
    'main_tables':'merged_socialdata_placesinfo_starbucksdata.csv' 
    }

    # Download all the files that we have in out bucket S3:

    for table in tables:
        if tables[table] in files:
            file = tables[table]
            logger.info(f'Processing table {table}....')
            df = download_csv_file(file)
            if table == 'main_tables':
                main_data = main_tables(df)
                for data in main_data:
                    df = checkDataframe(file,main_data[data])               
                    dictionary = df.to_dict('records')  
                    save_data(data,dictionary)
            else:
                df = checkDataframe(file,df)
                dictionary = df.to_dict('records')  
                save_data(table,dictionary)


if __name__ == '__main__':
    main_load()