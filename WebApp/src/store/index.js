import Vue from 'vue'
import Vuex from 'vuex'

import areas from './modules/areas'
import createLogger from '../plugins/logger'

Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'


export default new Vuex.Store({
  modules: {
    areas
  },
  strict: debug,
  plugins: debug ? [createLogger()] : []
})