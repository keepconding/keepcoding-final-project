from obtención_de_información_del_lugar import main_obtain
from merge_social_data_places_info_and_starbucks import main_merge
from load_database import main_load
from ampliar_información_del_dataset_de_Starbucks import main_info
import logging

LOG_LEVEL = logging.INFO
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=LOG_LEVEL)
logger = logging.getLogger(__name__)

# This function execute all the .py scripts in the correct order to process successfully all the datasets:
def main():
    logger.info('Starting main_obtain: Get the geopoints of the starbucks in New York...')
    main_obtain() # Get the geopoints of the starbucks in New York
    logger.info('Main obtain finished.')

    logger.info('Starting main_info: Get the most import point, places in each geopoint in New York...')
    main_info() # Get the most import point, places in each geopoint in New York.
    logger.info('Main info finished.')

    logger.info('Starting main_merge: Relation the starbucks information of each geopoint with the main points and places and the social demographic data in this geopoint...')
    main_merge() # Relation the starbucks's information of each geopoint with the main points and places and the social demographic data in this geopoint.
    logger.info('Main merge finished.')

    logger.info('Starting main_info: Get the most import point, places in each geopoint in New York...')
    main_load # Get process and save all the data in a DDBB.
    logger.info('Main load finished.')

if __name__ == '__main__':
    main()