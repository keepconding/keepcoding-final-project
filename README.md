# Trabajo Final

## Keepcoding project: [Starbucks](https://docs.google.com/presentation/d/1_iByhScvkztAftDruxb9jgcvi39jOG92re_xsq3iWMU/edit?usp=sharing)

Este proyecto tiene como objetivo principal crear un modelo predictivo que permita saber los benificios que obtendría un Starbucks en una determinada zona de New York en función de los parámetros social demográficos y en función de los disitntos puntos importantes que haya en esa zona o cuadrante.


### Definir Data Set

#### Elegir Data Set principal
El punto de partida de este proyecto, son 2 datasets que se pueden descargar de forma gratuita:
* Información de los distintos [starbucks de New York](https://gitlab.com/keepconding/keepcoding-final-project/-/blob/develop/Datasets/starbucks_long_island_original.csv).
* Información [social demográfica](https://gitlab.com/keepconding/keepcoding-final-project/-/blob/develop/Datasets/demographic_and_housing_data.csv) para cada cuadrante de  New York. 

Por otro lado, hemos creado desde cero un [Data Set de lugares relevantes  de New York](https://gitlab.com/keepconding/keepcoding-final-project/-/blob/develop/Datasets/New%20York_places_info.csv) en los distintos cuadrantes mediante la utilización de la [api de Google Street Map](https://gitlab.com/keepconding/keepcoding-final-project/-/blob/develop/Notebooks/Obtenci%C3%B3n_de_informaci%C3%B3n_del_lugar.ipynb).

Mediante la propia api de Google Street Map, somos capaces de [asignar puntos de geolocalización](https://gitlab.com/keepconding/keepcoding-final-project/-/blob/develop/Notebooks/Ampliar_informaci%C3%B3n_del_dataset_de_Starbucks.ipynb) a cada starbucks para poder situarlos en determinados cuadrantes y crear un [Data Set que incluya el cuadrante de cada starbucks](https://gitlab.com/keepconding/keepcoding-final-project/-/blob/develop/Datasets/new_york_starbucks.csv).


#### Cruzar fuentes de información
 Con este nuevo dataset que incluye los puntos geométricos de localización de cada starbucks, podemos relacionar los datos social demográficos, con los puntos de interés y la información de los starbucks que tenemos en cada cuadrante de New York.

Para ello, hemos creado un [código](http://localhost:8888/notebooks/Notebooks/Merge_Social_Data_places_info_and_Starbucks.ipynb) en python que se encarga de procesar los tres Data Set y unirlos a traves de los la geometría de localización, agrupando así toda la información por cuadrantes. 
Creando así un nuevo y unificado [Data Set](https://gitlab.com/keepconding/keepcoding-final-project/-/blob/develop/Datasets/merged_socialdata_placesinfo_starbucksdata.csv) donde contiene información de todos los cuadrantes de New York. Tanto de los que contienen un starbucks como de los que no tienen ninguno.

### [Arquitectura](https://miro.com/app/board/o9J_lPpgIcU=/)

#### Muestreo de datos
En este proyecto no contamos con datos que se vayan a actualizar de forma continua en cortos periodos de tiempo. Ya que no tiene sentido analizar cada mes si han abierto un starbucks nuevo en New York. 

Podriamos definir que es un ciclo que se renueva una vez al año. Por lo tanto va a ser un modelo que  maneje información que es prácticamente estática. 

#### Definir e implementar la arquitectura

Para la arquitectura nos hemos basado en un modelo tradicional que se suele emplear en  procesamiento de datos. Basado en tres partes principales:
* Origen de los datos. Dasatets de internet e información descargada mediante API.
* Un bucket en AWS donde guardamos toda la información en bruto.
* Una BBDD PostgreSQL donde vamos a guardar la informació una vez haya sido procesada.


##### Origen de datos
Inicialmente tenemos unos Data Sets en formato .csv que descargamos desde internet y otros que creamos desde cero mediante la API de Google Street Map.

Dichos Data Sets en formato .csv los descargamos a mano y los guardamos en nuestro Bucket. También prodiamos automatizar esta primera acción de descargar y posteriormente guardar la información en un Bucket mediante script de python.

##### Bucket AWS
A partir de aquí empiezan todos los procesos de procesamiento y mezcla de de Data Sets. 

Cada uno de estos procesos concluye con la creación de un archivo, normalmente en formato .csv. 
Creamos distintos archivos en dichos procesamientos:
* El .csv de los lugares relevantes de New York.
* El merge de todos los Data Sets que tenemos.

Cada uno de estos archivos se guardan automáticamente en el Bucket sin necesidad de acción humana.

¿Que ventajas tiene el Bucket? 
* Tener toda la información agrupada en un mismo sitio.
* Asegurarte de que esa información no las vas a perder nunca.
* Dar acceso a los archivos a todas las personas del equipo desde distintos ordenadores.
* Permitir a distintas personas que estén autorizadas a guardar o borrar información si fuera necesario.

##### BBDD

Las BBDD tienen mútiples  ventajas:
* Aseguraros de que se guardan ciertas relaciones entre los datos que tenemos.
* Nos permiten tener estructurada la información. Lo cual facilita las búsquedas.

En este caso tenemos una BBDD de PostgreSQL montada en AWS. Esto nos asegura no perder nunca la información y permite en función de los roles que asignemos, dar acceso y permisos a distintas personas del equipo.

En este caso es una base de datos relacional cuya [estructura](https://gitlab.com/keepconding/keepcoding-final-project/-/blob/develop/Databases/create_tables.sql) consiste en  
tres tablas principales unidas entre si a través de otra tabla que ejerce de nodo.

#### Ingesta de datos

En este caso hemos creado un [ETL](https://gitlab.com/keepconding/keepcoding-final-project/-/blob/develop/Notebooks/Load_Database.ipynb) (Extraction, Transform and Load Data) con python.

El proceso consiste en coger desde el bucket el .csv donde tenemos toda la [información](https://gitlab.com/keepconding/keepcoding-final-project/-/blob/develop/Datasets/merged_socialdata_placesinfo_starbucksdata.csv) (starbucks, puntos de interés e información social demográfica) junta.

En la ETL realiza los siguientes procesos:
* Conectarse al bucket de AWS.
* Conectarse a la BBDD de AWS.
* Descargar .csv principal desde el bucket.
* Procesar la información, separarla, filtrarla,, modificarla y guardarla en la BBDD.

Como las tablas de la BBDD está relacionadas entre si, es necesario que la ingesta de datos se lleve a cabo en un determinado orden. Pero todo esto se encarga la propia ETL.

#### Validazicón de la calidad de los datos

La BBDD está diseñada para que se guarden estrictas relaciones entre las diversas tablas para asegurar la estabilidad de la BBDD.
Obviamente, una vez tenemos la información en la BBDD, hacemos pruebas mediante queries para asegurarnos de que todo está correcto.

### Análisis exploratorio

#### Detección de outliers, imputación valores nulos.

Llegados a este punto de análisis de la información, decidimos realizar dos estudios separados:
* Análisis del [Data Set social demográfico](https://gitlab.com/keepconding/keepcoding-final-project/-/blob/develop/Datasets/demographic_and_housing_data.csv)
* Análisis del [Data Set de puntos de interés de New York](https://gitlab.com/keepconding/keepcoding-final-project/-/blob/develop/Datasets/New%20York_places_info.csv) 

[Análisis de Data Set social demográfico](https://gitlab.com/keepconding/keepcoding-final-project/-/blob/develop/Notebooks/EDA_sociodemohgraphic_data%20(2).ipynb)

En este caso, teníamos un [Data Set](https://gitlab.com/keepconding/keepcoding-final-project/-/blob/develop/Datasets/demographic_and_housing_data.csv) que contaba con demasiadas columnas.
La estrategia para eliminar todas las columnas innecearias fue agruparlas por tipo. Población, salarios, etnia y edad.

Una vez creados estos grupos, analizamos cada uno de estos individualmente, mirando la relación que tenían entre ellas las columnas de cada grupo y analizando la relación de cada una de ellas con la variable **revenue**, la cual hace referencia a los beneficios obtenidos por cada starbucks. Es decir, la variable que queremos predecir.

En función de los resultados obtenidos, fuimos eliminando columnas que guardaban mucha correlación entre si o muy poca correlación con la variable **reveneu**.

También eliminamos outliers aplicando una variable de distribuición directamente sobre la variable **revenue**.

Finalmente, con las columnas que eran válidas para el modelo, obtuvimos un nuevo [Data Set](https://gitlab.com/keepconding/keepcoding-final-project/-/blob/develop/Datasets/SocialDemographicAnalyze.csv).

[Análisis de los lugares más relevantes de New York](https://gitlab.com/keepconding/keepcoding-final-project/-/blob/develop/Notebooks/Exploratory_Analysis_Places_Info.ipynb)

El análisis de este Data Set fue más sencillo, ya que era información que nosotros mismos habíamos seleccionado y descargado mediante la API de Google Street Map.

Por lo tanto, carecía de valores nulos o de campos innecesarios para el modelo.

### Preprocesamiento

El pre procesamiento ha consistido en dividir el data set en train y test.
Como teniamos todos los data sets unidos, hemos empleado para el entrenamiento la información de las zonas donde si teníamos un starbucks.

Hemos llevado a cabo una normalización mediante un escalado y un PCA con la que hemos reducido todas las variables a un total de cinco.

En este caso no ha sido necesario combinar ni categorizar variables. El data set ya contenía originalmente toda la información que necesitábamos.

### [Modelado](https://gitlab.com/keepconding/keepcoding-final-project/-/blob/master/Notebooks/main.ipynb)

El modelo ha sido creado mediante mediante PCA y los resultados son de carácter binario.
* Si es un sitio propicio para un colocar un Starbucks el resultado será 1.
* Si el sitio NO es propicio será un un 0.

Tenemos un accuracy del 80% por lo tanto podemos ar por bueno el modelo.

### Informe

A lo largo de este proyecto hemos aprendido:

* Hay que tener una visión global de lo que se quiere hacer. Una idea, un objetivo.
*  No es fácil encontrar data sets el data set que buscas y tener acceso a el.
* Antes de empezar a desgranar el proyecto en pequeñas partes. Necesitamos comprobar si contamos con los data sets qe vamos a necesitar.

* Definir las diversas partes que va a tener el proyecto y en que orden las vamos a crear.

* Dividir el trabajo en función de las motivaciones y conocimientos de cada componente del equipo. Buscando siempre una repartición equitativa para acortar plazos y mejorar el workflow del proyecto.

* Mantener una comunicación fluida en el equipo para ver si hay partes bloqueadas o confirmar que todo va correcatamente.

* Aportar siempre ideas propias que puedan ser útiles de cara al desarrollo del proyecto.

* No preocuparse si a veces hay que dar un paso a trás para seguir avanzando hacia adelante.

* Hacer bien las cosas, intentar hacer un trabajo que sea entendible y fácil de seguir desde fuera.

* Dar visualización a todo lo  que hacemos. Ya sea con diagramas, excels, readmes.

* Dar visualización a los datos para que cualquier persona tengo un acceso rápido y sencillo. 

* Es importante dar acceso desde fuera a nuestro código para que pueda ser utilizado por personas ajenas al proyecto.

* No fijarte solo en tu parte. Es importante tener una imagen global del proyecto y ver que hacen tus compañeros y compañeras para entender de verdad en que punto está el proyecto.

#### [Resultados](https://gitlab.com/keepconding/keepcoding-final-project/-/blob/master/Results/Results_presentation.ipynb)

Estamos contentos con el modelo que hemos creado, funciona correctamente y cumple con el objetivo que nos habiamos marcad0 inicialmente. Predecir si merece la pena o no colocar un Starbucks en una determinada zona de New York.

Tenemos un modelo predictivo binario con un accuracy del 80%. 