import mergedata from '../../api/mergedata'


// initial state
const state = () => ({
  all: ''
})

// getters
const getters = {}

// actions
const actions = {
  getAllAreas({
    commit
  }) {
    mergedata.getCSVMergedData().then(function(areas) {
      commit('setAreas', areas)
    })
  }
}

// mutations
const mutations = {
  setAreas(state, areas) {
    state.all = areas
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}