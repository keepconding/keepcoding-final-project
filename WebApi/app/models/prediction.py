from pydantic import BaseModel


class StarbucksPredictionResult(BaseModel):
    score: float
    elapsed_time: float
