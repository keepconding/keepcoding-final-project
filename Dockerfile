FROM python:3.8.3-slim-buster

RUN apt-get update -y && \
    apt-get install build-essential -y && \
    pip install --upgrade pip

RUN apt-get update &&\
    apt-get install -y binutils libproj-dev gdal-bin

COPY ./requirements.txt /
RUN pip3 install -r requirements.txt

RUN pip install psycopg2-binary

RUN apt-get install -y libspatialindex-dev

COPY ./Docker/pythonFiles /Docker/pythonFiles

COPY ./Datasets /Datasets

CMD [ "python3", "./Docker/pythonFiles/main.py" ]
