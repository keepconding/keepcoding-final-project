import Vue from 'vue'
import './plugins/axios'
import App from './App.vue'
import VuePapaParse from 'vue-papa-parse'
import vuetify from './plugins/vuetify';
import store from './store'

Vue.config.productionTip = false

Vue.use(VuePapaParse)

new Vue({
  vuetify,
  store,
  render: h => h(App)
}).$mount('#app')