import os
import time
import pickle

import sklearn

from loguru import logger

from app.core.messages import NO_VALID_PAYLOAD
from app.models.payload import StarbucksPayload, payload_to_array
from app.models.prediction import StarbucksPredictionResult

from app.core.config import (
    MODEL,
    MODEL_PCA
)


class StarbucksModel:
    def __init__(
        self, model_dir,
    ):
        self.model_dir = model_dir
        self._load_local_model()

    def _load_local_model(self):
        model_path = os.path.join(self.model_dir, MODEL)
        with open(model_path, 'rb') as file:
            self.model = pickle.load(file)
        model_path_pca = os.path.join(self.model_dir, MODEL_PCA)
        with open(model_path_pca, 'rb') as file:
            self.pca = pickle.load(file)

    def _predict(self, prediction_values: list) -> float:
        transformed_values = self.pca.transform([prediction_values])
        logger.debug(transformed_values)
        prediction = self.model.predict(transformed_values)
        logger.debug(prediction)
        return prediction[0]

    def _post_process(
        self, prediction: float, start_time: float
    ) -> StarbucksPredictionResult:
        logger.debug("Post-processing prediction.")

        return StarbucksPredictionResult(
            score=prediction, elapsed_time=(time.time() - start_time)
        )

    def predict(self, payload: StarbucksPayload):
        if payload is None:
            raise ValueError(NO_VALID_PAYLOAD.format(payload))

        start_at = time.time()
        pre_processed_payload = payload_to_array(payload)
        prediction = self._predict(pre_processed_payload)
        logger.debug(prediction)
        post_processed_result = self._post_process(
            prediction, start_at
        )

        return post_processed_result
