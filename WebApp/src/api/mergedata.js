import file from '../assets/csv/merged_socialdata_starbucksdata.csv'

export default {
  getCSVMergedData() {
    return new Promise(function(myResolve) {
      myResolve({
        file
      })
    });

  }
}