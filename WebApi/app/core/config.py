import os

APP_VERSION = "0.0.1"
APP_NAME = "Starbucks prediction API"
API_PREFIX = "/api"

IS_DEBUG = os.getenv("IS_DEBUG", True)
# DEFAULT_MODEL_PATH = os.getenv("DEFAULT_MODEL_PATH")
DEFAULT_MODEL_PATH = "../Models/"

# EXPORT
MODEL = "starbucks_classifier.pkl"
MODEL_PCA = "starbucks_classifier_pca.pkl"


