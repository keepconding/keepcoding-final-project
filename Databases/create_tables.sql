create table new_york_zipcodes(
	zipcode int primary key,
	city varchar,
	latitude float,
	longitude float 
);

create table zipcode_data(
	id int not null primary key,
	zipcode int not null
);

create table new_york_starbucks_information(
	storenumber bigint not null primary key , 
	name varchar (100) not null,
	hours_open_per_week float not null,
	gc_status_rel float not null, 
	has_lu float not null, 
	has_cl float not null, 
	has_wa float not null, 
	has_vs float not null,  
	revenue float not null , 
	lat float not null, 
	lng float not null	
);

create table new_york_places(
	id int not null primary key,
	aerports int not null,	
	hostelry int not null,
	schools	int not null,
	banks int not null,	
	sanity int not null,	
	spectacles int not null,	
	police_stations	int not null,
	temples	int not null,
	stadiums int not null,	
	stations int not null,	
	malls int not null,	
	fashion_shops int not null,	
	parks int not null,	
	gyms int not null,	
	hotels int not null,	
	attractions int not null,
	constraint id_zipcode_places FOREIGN KEY(id) REFERENCES zipcode_data(id)

);


create table new_york_demographic(
	id int not null primary key,
	nonfamiliy_households_male_householder_living_alone int not null,
	nonfamiliy_households_female_householder_living_alone int not null,
	total_hispanic_or_latino_population int not null,
	total_householders int not null,
	family_size2_person_families int not null,
	number_of_earners_1_earner int not null,
	population_age_25_to_34_years int not null,
	population_age_35_to_44_years int not null,
	householder_by_age_25_to_44_years int not null,
	estimate_total_housing_units int not null,
	constraint id_zipcode_social FOREIGN KEY(id) REFERENCES zipcode_data(id)
);

create table starbucks(
	id int not null,
	storenumber bigint not null,
	primary key(id,storenumber),
	constraint storenumber FOREIGN KEY(storenumber) REFERENCES new_york_starbucks_information(storenumber),
	constraint id_zipcode FOREIGN KEY(id) REFERENCES zipcode_data(id)
)