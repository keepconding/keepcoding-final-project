import geopandas
import pandas as pd
import json
import requests
import os
import boto3


def read_csv():
    df = pd.read_csv('./Datasets/starbucks_long_island_original.csv')
    return df

#import copy
#df_new_york = copy.deepcopy(df[df['City'] == 'New York'])



def obtain_position_data(street_address, city):
    API_KEY = 'AIzaSyDmJuJMPA3kbQ98Sfh27GbbMGcaoNLU24Y' #Key para utilizar sin desfase
    url = 'https://maps.googleapis.com/maps/api/geocode/json?address=Starbucks,'+street_address+','+ city + '&key=' + API_KEY
    response = requests.get(url)
    json_data = json.loads(response.text)
    if len(json_data['results']) > 0:
        lat = json_data['results'][0]['geometry']['location']['lat']
        lng = json_data['results'][0]['geometry']['location']['lng']
        return (lat,lng)
    else:
        return (pd.NA, pd.NA)

def obtain_info(row):
    #return obtain_position_data(row['addresslines'],row['City'])
    return obtain_position_data(row['addresslines'],'New York')


def save_bucket():
    #Connection with the bucket S3:
    s3 = boto3.resource(
        service_name='s3',
        region_name='eu-west-1',
        aws_access_key_id='AKIAYUYRMI3JJJ6SRUMY',
        aws_secret_access_key='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'
    )

    my_bucket = s3.Bucket('keepcoding')


    # Save or upload the .csv in our bucket:
    s3.Object('keepcoding', 'new_york_starbucks.csv').put(Body=open('./Datasets/new_york_starbucks.csv', 'rb'))

def main_info():

    df = read_csv()
    df['lat'], df['lng'] = zip(*df.apply(obtain_info, axis=1))
    df_new_york = geopandas.GeoDataFrame(df, geometry=geopandas.points_from_xy(df.lng, df.lat))
    df_new_york.to_csv('./Datasets/new_york_starbucks.csv')
    save_bucket()

if __name__ == '__main__':
    main_info()

