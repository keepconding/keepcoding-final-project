import pandas as pd
import overpy
import time
import boto3
api = overpy.Overpass()


# ## Carácteristicas de los mapas
# [https://wiki.openstreetmap.org/wiki/Map_features](https://wiki.openstreetmap.org/wiki/Map_features)
# En este link podemos ver las carácteristicas que se pueden sacar a los mapas.
# Abajo las carácteristicas que se me han ocurrido, pero se pueden añadir o modificar.

# Connection with the bucket
s3 = boto3.resource(
      service_name='s3',
      region_name='eu-west-1',
      aws_access_key_id='AKIAYUYRMI3JJJ6SRUMY',
      aws_secret_access_key='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'
  )

my_bucket = s3.Bucket('keepcoding')

characteristics = {
    "aerports":("aeroway" ,["terminal"]),
    "hostelry":("amenity",["bar", "bbq", "biergaten","cafe","fast_food", "pub", "restaurant"]),
    "schools": ("amenity",["library", "school", "university", "college"]),
    "banks" : ("amenity",["bank", "atm"]),
    "sanity" : ("amenity", ["clinic", "hospital","pharmacy"]),
    "spectacles": ("amenity", ["cinema", "theatre"]),
    "police_stations" : ("amenity",["police"]),
    "temples": ("building", ["temple"]),
    "stadiums": ("leisure", ["stadium"]),
    "stations":("public_transport",["station"]),
    "malls": ("shop", ["department_store","mall"]),
    "fashion_shops":("shop",["bag", "boutique", "clothes","fashion_accesories","leather","jewelry", "shoes", "perfumery"]),
    "parks": ("leisure",["park"]),
    "gyms":("leisure",["fitness_centre", "sports_centre", "multi"]),
    "hotels":("tourism",["hotel","hostel","guest_house"]),
    "attractions": ("tourism",["attraction"])
}


def get_characteristic_elements(city, characteristic):
  query = "[out:json];" 
  query = query + "area[name=\"" + city + "\"]->.searchArea;"
  for characteristic_type in characteristic[1]:
    query = query + "node[\""+characteristic[0]+"\"=\""+characteristic_type+"\"](area.searchArea);out;"
    query = query + "way[\""+characteristic[0]+"\"=\""+characteristic_type+"\"](area.searchArea);out center;"
    query = query + "relation[\""+characteristic[0]+"\"=\""+characteristic_type+"\"](area.searchArea);out center;"
  result = api.query(query)
  return result


def get_characteristic_elements_by_city(city, characteristics):
  places = [["type", "name", "latitude", "longitude"]]
  for key in characteristics:
    try:
      result = get_characteristic_elements(city,characteristics[key])
    except: 
      print("Too many request... waiting a bit and then I am trying again")
      time.sleep(15) #Si se hacen muchas llamadas seguidas no deja y necesita un descanso de 10 segundos
      print("Trying again")
      try:
        result = get_characteristic_elements(city,characteristics[key])
      except:
        print("5 minute wait and then again")
        time.sleep(300) #Si se hacen muchas llamadas seguidas no deja y necesita un descanso de 10 segundos
        result = get_characteristic_elements(city,characteristics[key])
    print(key + " from the city " + city + " obtained")
    print("Nodes: " + str(len(result.nodes)) + " Ways: " + str(len(result.ways)) + " Relations: " + str(len(result.relations)))
    for node in result.nodes:
      if "name" in node.tags:
        name = node.tags['name']
      else:
        name = node.tags[list(node.tags.keys())[0]]
      latitude = node.lat
      longitude = node.lon
      places.append([key,name,latitude,longitude])
    for way in result.ways:
      if "name" in way.tags:
        name = way.tags['name']
      else:
        name = way.tags[list(way.tags.keys())[0]]  
      latitude = way.center_lat
      longitude = way.center_lon
      places.append([key,name,latitude,longitude])
    for relation in result.relations:
      if "name" in relation.tags:
        name = relation.tags['name']
      else:
        name = relation.tags[list(relation.tags.keys())[0]]  
      latitude = relation.center_lat
      longitude = relation.center_lon
      places.append([key,name,latitude,longitude])    
  return places

def main_obtain():

  cities = ["New York"]
  for city in cities:
      places = get_characteristic_elements_by_city(city, characteristics)
      pd.DataFrame(places).to_csv(f'./Datasets/{city}_places_info.csv')
      
      # Save or upload the .csv in our bucket:
      s3.Object('keepcoding', f'{city}_places_info.csv').put(Body=open(f'./Datasets/{city}_places_info.csv', 'rb'))


if __name__ == '__main__':
    main_obtain()