import pandas as pd
import geopandas as gpd
from shapely import wkt
import boto3
import warnings
warnings.filterwarnings("ignore")

# Combinamos los datos de geojson con los elementos geograficos

def combine_data_geojson_info_with_categories(neightborghood):
  amount_of_items = {}
  for column in places_categories:
    amount_of_items[column] = 0
  for index, place in df_places.iterrows():     
    if is_place_in_neghtborghood(neightborghood.geometry, place.latitude, place.longitude):
      amount_of_items[place.type] = amount_of_items[place.type] + 1
  response = []
  for i in range(len(places_categories)):
    response.append(amount_of_items[places_categories[i]])
  return  pd.Series(response)

# Data sociodemografica
def get_zipcode(name):
  zipcode = name[6:]
  return zipcode

def get_data_ready(dataframe, medida = None):
  #poner las descripciones de la columna como nombres: 
  dataframe.columns = dataframe.iloc[0]
  dataframe.drop(dataframe.index[0], inplace=True)

  #en "Geographic Area Name" nos quedamos solo con el zipcode:
  dataframe['Geographic Area Name'] = list(map(lambda x: get_zipcode(x), dataframe['Geographic Area Name']))
  aux = dataframe['Geographic Area Name']
  #eliminamos todas las columnas que indican margen de error:
  dataframe = dataframe.loc[:,(dataframe.columns.str.startswith('Estimate'))]
  if medida != None:
    dataframe = dataframe.loc[:,(dataframe.columns.str.contains(medida))]

  dataframe['Geographic Area Name'] = aux
  cols = dataframe.columns.tolist()
  cols.insert(0, cols.pop(cols.index('Geographic Area Name')))
  dataframe = dataframe[cols]

  return dataframe

def save_bucket():
  #Connection with the bucket S3:
  s3 = boto3.resource(
      service_name='s3',
      region_name='eu-west-1',
      aws_access_key_id='AKIAYUYRMI3JJJ6SRUMY',
      aws_secret_access_key='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'
  )

  my_bucket = s3.Bucket('keepcoding')

  # Save or upload the .csv in our bucket:
  s3.Object('keepcoding', 'merged_socialdata_placesinfo_starbucksdata.csv').put(Body=open('../../Datasets/merged_socialdata_placesinfo_starbucksdata.csv', 'rb'))

def main_merge(): 

  data_geojson = gpd.read_file('./Datasets/ny_new_york_zip_codes_geo.min.json')

  dataset_places_info= "./Datasets/New York_places_info.csv"

  df_places = pd.read_csv(dataset_places_info, sep=",", quotechar='"',header=1, error_bad_lines=False)

  df_places.dropna(inplace=True)
  df_places['is_long_island'] = df_places.apply(is_in_long_island, axis=1)
  df_places = df_places[df_places['is_long_island']==True]

  places_categories = df_places["type"].unique()
  places_categories

  newcols = data_geojson.apply(combine_data_geojson_info_with_categories, axis=1)

  newcols.columns = places_categories

  newcols.describe()

  data_geojson = data_geojson.merge(newcols,left_index=True, right_index=True)

  # Data "mean income in past 12 months"
  data_income = pd.read_csv('./Datasets/mean_income_12_months.csv')
  data_income = get_data_ready(data_income, medida = 'Number' )

  # Data "demographic and housing estimates"
  data_demo_housing = pd.read_csv('./Datasets/demographic_and_housing_data.csv')
  data_demo_housing = get_data_ready(data_demo_housing)

  data_demo_housing.head(3)

  # Merge to get all data
  data = pd.merge(data_income, data_demo_housing, on='Geographic Area Name')


  # Merge with geojson
  geojson_columns_we_want = ['ZCTA5CE10', 'geometry'] + places_categories.tolist()
  data_final = pd.merge(data_geojson[geojson_columns_we_want],data, left_on='ZCTA5CE10', right_on='Geographic Area Name')
  data_final.drop('ZCTA5CE10', axis = 1, inplace=True)

  # Merge with data starbucks
  data_starbucks = pd.read_csv('./Datasets/new_york_starbucks.csv')
  data_starbucks['geometry'] = data_starbucks['geometry'].apply(wkt.loads)
  data_starbucks = gpd.GeoDataFrame(data_starbucks)

  data_starbucks.plot(alpha=0.5, edgecolor='k', cmap='tab10')

  data_final = gpd.GeoDataFrame(data_final)
  data_final.plot(alpha=0.5, edgecolor='k', cmap='tab10')

  data_final = data_final.set_crs(epsg=4326)
  data_starbucks = data_starbucks.set_crs(epsg=4326)

  stores_enriched = gpd.sjoin(data_starbucks[['storenumber','revenue','geometry']], data_final, how='right', op='intersects').drop('index_left', axis = 1)

  len(stores_enriched.storenumber.unique())
  len(stores_enriched['Geographic Area Name'].unique())

  stores_enriched.to_csv('./Datasets/merged_socialdata_placesinfo_starbucksdata.csv', index=False)

  save_bucket()

if __name__ == '__main__':
    main_merge()