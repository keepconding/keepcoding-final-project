from typing import Callable

from fastapi import FastAPI
from loguru import logger

from app.services.models import StarbucksModel
from app.core.config import (
    DEFAULT_MODEL_PATH
)


def _startup_model(app: FastAPI) -> None:
    model_direction_path = DEFAULT_MODEL_PATH
    model_instance = StarbucksModel(model_dir=model_direction_path)
    app.state.model = model_instance


def _shutdown_model(app: FastAPI) -> None:
    app.state.model = None


def start_app_handler(app: FastAPI) -> Callable:
    def startup() -> None:
        logger.info("Running app start handler.")
        _startup_model(app)

    return startup


def stop_app_handler(app: FastAPI) -> Callable:
    def shutdown() -> None:
        logger.info("Running app shutdown handler.")
        _shutdown_model(app)

    return shutdown
